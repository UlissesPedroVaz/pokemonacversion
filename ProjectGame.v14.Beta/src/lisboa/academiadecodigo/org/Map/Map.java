package lisboa.academiadecodigo.org.Map;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Map {

    private Rectangle background, transition;

    private Picture map1, dialog1, dialog2, dialog1P, dialog2P, dialog1Defeated, dialogNPC2ready,  dialog1Correct, sorry;

    private Text text1, text2, text1P, text2P, text1Defeated, text2Defeated, text1NotReady, text2NotReady, text1Correct, text2Correct, nextLevel, nextLevel2;

    public static final int COLS = 8;
    public static final int ROWS = 6;

    public static final int CELL_SIZE = 20;
    public static final int PADDING = 10;

    // Construtor
    public Map() {

        background = new Rectangle(PADDING, PADDING, getWidth(), getHeight());
        map1 = new Picture(PADDING, PADDING, "Background.v2.png");


        background.fill();
        map1.draw();

    }
        // Fala o NPC1
    public void dialog1NPC() {

        dialog1 = new Picture(230, 220, "text.png");
        dialog1.draw();
        dialog1.grow(30, 0);

        text1 = new Text(240, 240, "Hello, my name is Sara");
        text2 = new Text(240, 260, "Welcome to Academia de Codigo!!");
        text1.draw();
        text2.draw();
        text1.grow(5, 5);
        text2.grow(5, 5);
    }

    // Fala do player
    public void dialog1Player() {
        System.out.println("Fez player Dialoge");
        dialog1.delete();
        text1.delete();
        text2.delete();
        dialog1P = new Picture(5, 250, "text2-2.png");
        dialog1P.draw();
        dialog1P.grow(-10, 2);

        text1P = new Text(35, 260, "Hi mate!");
        text2P = new Text(25, 280, "Nice to meet you!?");
        text1P.draw();
        text2P.draw();
        text1P.grow(5, 5);
        text2P.grow(5, 5);
    }

    // Fala NCP1 quando ja esta Defeated
    public void dialogNPC1Defeat() {

        dialog1Defeated = new Picture(230, 220, "text.png");
        dialog1Defeated.draw();
        dialog1Defeated.grow(30, 0);

        text1Defeated = new Text(240, 240, "Noooo i'm defeated");
        text2Defeated = new Text(240, 260, "GO AWAY!!!");
        text1Defeated.draw();
        text2Defeated.draw();
        text1Defeated.grow(5, 5);
        text2Defeated.grow(5, 5);
    }

    // NPC Talk if is not ready for combat
    public void dialogNPC2notReady() {

        dialogNPC2ready = new Picture(300, 120, "text2.png");
        dialogNPC2ready.draw();
        dialogNPC2ready.grow(30, 0);

        text1NotReady = new Text(330, 140, "Hey!! Go away!");
        text2NotReady = new Text(300, 160, "You need to do the first challenge");
        text1NotReady.draw();
        text2NotReady.draw();
        text1NotReady.grow(5, 5);
        text1NotReady.grow(5, 5);
    }

    // Fala NCP1 ja acertou uma resposta
    public void dialogNPC1answer1() {

        dialog1Correct = new Picture(230, 220, "text.png");
        dialog1Correct.draw();
        dialog1Correct.grow(30, 0);

        text1Correct = new Text(240, 240, "Well, I see you are lucky");
        text2Correct= new Text(240, 260, "Wanna try again?");
        text1Correct.draw();
        text2Correct.draw();
        text1Correct.grow(5, 5);
        text2Correct.grow(5, 5);
    }


    public void dialog2NPC() {
        System.out.println("Fez NPC2 Dialoge");
        dialog2 = new Picture(300, 120, "text2.png");
        dialog2.draw();
        dialog2.grow(30, 0);

        text1 = new Text(330, 140, "Hello, my name is Rolo");
        text2 = new Text(300, 160, "I see you have past the first challenge");
        text1.draw();
        text2.draw();
        text1.grow(5, 5);
        text2.grow(5, 5);
    }

    // Fala do player no NPC2
    public void dialog2Player() {
        System.out.println("Fez player Dialoge2");
        dialog2.delete();
        text1.delete();
        text2.delete();
        dialog2P = new Picture(650, 140, "text3.png");
        dialog2P.draw();
        dialog2P.grow(-20, 0);

        text1P = new Text(710, 150, "Yes!!");
        text2P = new Text(700, 170, "I'm the Best");

        text1P.draw();
        text2P.draw();
        text1P.grow(5, 5);
        text2P.grow(5, 5);

    }

    public void dialog3NPC() {
        text1.delete();
        text2.delete();
        dialog1P.delete();
        dialog1.delete();
        text1P.delete();
        text2P.delete();

        dialog1.draw();
        dialog1.grow(0, 0);

        text1 = new Text(255, 240, "Are you ready for some Adventure?");
        text2 = new Text(250, 260, "Press Space > START | Press Q > QUIT");
        text1.draw();
        text2.draw();
        text1.grow(5, 5);
        text2.grow(5, 5);
    }

    public void dialog4NPC() {
        text1.delete();
        text2.delete();
        dialog2.delete();
        dialog2P.delete();
        dialog2.delete();
        text1P.delete();
        text2P.delete();

        dialog2.draw();
        dialog2.grow(30, 0);

        text1 = new Text(285, 140, "You are a tuff guy, want more?");
        text2 = new Text(280, 160, "Press Space > START | Press Q > QUIT");
        text1.draw();
        text2.draw();
        text1.grow(5, 5);
        text2.grow(5, 5);
    }

    // Dialogo com o NPC1
    public void removedialog1(int clickedTimes) {
        System.out.println("Entrou no remove dialog");
        if (clickedTimes == 1) {
            dialog1.delete();
            text1.delete();
            text2.delete();
            return;
        } else if (clickedTimes == 2) {
            dialog1P.delete();
            text1P.delete();
            text2P.delete();
            return;
        } else if (clickedTimes == 2){
            dialog1.delete();
            text1.delete();
            text2.delete();
        }
    }

    // Dialogo com o NPC2
    public void removedialog2(int clickedTimes) {
        if (clickedTimes == 1) {
            dialog2.delete();
            text1.delete();
            text2.delete();
            return;
        } else if (clickedTimes == 2) {
            dialog2P.delete();
            text1P.delete();
            text2P.delete();
            return;
        } else if (clickedTimes == 3) {
            dialog2.delete();
            text1.delete();
            text2.delete();
        }
    }

    // Remove dialog de NPC defeat:
    public void removeNPCdefeatDialog() {
        dialog1Defeated.delete();
        text1Defeated.delete();
        text2Defeated.delete();
    }

    // Remove dialog de NPC1 1 question right
    public void removeNPC1ansewrRight() {
        dialog1Correct.delete();
        text1Correct.delete();
        text2Correct.delete();
    }

    // Remove dialog de NPC notReady:
    public void removeNPCnotReady() {
        dialogNPC2ready.delete();
        text1NotReady.delete();
        text2NotReady.delete();
    }

    // Apaga o BackGround
    public void erase() {
        System.out.println("Apagou o Background");
        map1.delete();
    }

    // Desenha o BackGround
    public void show() {
        System.out.println("Desenhou o Background");
        map1.draw();
    }

    public void NextLevel() {
        transition = new Rectangle(10, 10, 800, 600);
        transition.setColor(Color.BLACK);
        transition.fill();

        sorry = new Picture(400,400, "sorry.png");
        sorry.draw();

        nextLevel = new Text(300, 230, "You win this level but...");
        nextLevel.setColor(Color.WHITE);
        nextLevel.grow(70, 60);
        nextLevel.draw();

        nextLevel2 = new Text(300, 300, "We are working on next Levels ...");
        nextLevel2.setColor(Color.WHITE);
        nextLevel2.grow(50, 40);
        nextLevel2.draw();

    }

    //GET COL
    public int getCols() {
        return COLS;
    }

    public int getRows() {
        return ROWS;
    }

    // Get width
    public int getWidth() {
        return COLS * CELL_SIZE;
    }

    // Get Height
    public int getHeight() {
        return ROWS * CELL_SIZE;
    }

    // get Cell-size
    public int getCellSize() {
        return CELL_SIZE;

    }


}
