package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import java.util.Timer;
import java.util.TimerTask;

public class Combat {

    private String name;
    private boolean inCombat;

    private int npcHealth, playerHealth, npcHealth1, playerHealth1;
    private int rewind = 0;

    private Rectangle timer1, timer2, health1, health2, health3, health4;
    private Picture frame, background, result;
    private Text quiz, answer;

    private KeyboardControl keyboard;
    private Map map;
    private Game game;

    private Player player;
    private NPC npc;


    public Combat(Game game, Map map, Player player, NPC npc) {
        this.game = game;
        this.map = map;
        this.player = player;
        this.npc = npc;
    }

    // Acrescentado:
    // Getter inCombat:
    public boolean isInCombat() {
        return inCombat;
    }


    public void startCombat() {

        // Apaga o mapa
        map.erase();
        System.out.println("Fez erase do mapa");

        // Apaga o Player
        player.erasePalyer();
        System.out.println("Fez erase do player");


        // Apaga os NPCS
        npc.eraseNPC();


        if (!inCombat) {
            System.out.println("Entrou no while Combat");
            inCombat = true;

            //quiz frames
            frame = new Picture(10, 10, "frame2.png");
            frame.draw();

            //quiz time //width time and health
            timer1 = new Rectangle(30, 170, 278, 10);
            timer1.setColor(Color.CYAN);
            timer1.fill();
            timer2 = new Rectangle(29, 169, 279, 11);
            timer2.setColor(Color.BLACK);
            timer2.draw();

            //NPC health //width health
            health1 = new Rectangle(30, 180, 278, 10);
            health1.setColor(Color.RED);
            health1.fill();
            health2 = new Rectangle(29, 179, 279, 11);
            health2.setColor(Color.BLACK);
            health2.draw();

            //Player health //width health
            health3 = new Rectangle(511, 440, 278, 10);
            health3.setColor(Color.RED);
            health3.fill();
            health4 = new Rectangle(510, 439, 279, 11);
            health4.setColor(Color.BLACK);
            health4.draw();

            //Text quiz
            quiz = new Text(60, 60, "Question");
            quiz = new Text(60, 80, "Question");
            quiz.draw();

            //Answers
            answer = new Text(550, 500, "Hello this is a test, to see if the");
            answer.draw();




            /*combat();
            boolean timer = game.isTimer();
            timer = true;*/
        }
    }

    public void question1() {
        System.out.println("Entered Question 1");
        System.out.println("Player Health " + playerHealth + " Health3 width " + health3.getWidth());
        System.out.println("NPC Health " + npcHealth + " Health1 width " + health1.getWidth());
        switch (game.getPressedKey()) {
            case 1:
                //Wrong answer
                System.out.println("Pressed 1");
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 2:
                //Right answer
                System.out.println("Pressed 2");
                npcHealth = health1.getWidth() - 140;
                health1.delete();
                health1 = new Rectangle(health1.getX(), health1.getY(), npcHealth, 10);
                health1.setColor(Color.RED);
                health1.fill();
                if (npcHealth < 140) {
                    result = new Picture(100, 270, "next.png");
                    result.draw();
                    game.setQuestion1(true);
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    question2();
                                }
                            },
                            2000
                    );
                }
                break;
            case 3:
                //Wrong answer
                System.out.println("Pressed 3");
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 4:
                //Wrong answer
                System.out.println("Pressed 4");
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
        }


    }

    public void question2() {
        System.out.println("Entered Question 2");
        System.out.println("Player1 Health " + playerHealth + " Health3 width " + health3.getWidth());
        System.out.println("NPC1 Health " + npcHealth + " Health1 width " + health1.getWidth());

        if(rewind == 0) {
            rewind++;
            frame = new Picture(10, 10, "frame2.png");
            frame.draw();
            drawBar();
        }

        switch (game.getPressedKey()) {
            case 1:
                //Wrong answer
                System.out.println("Pressed 1");
                System.out.println("Player1 Health " + playerHealth + " Health3 width " + health3.getWidth());
                System.out.println("NPC1 Health " + npcHealth + " Health1 width " + health1.getWidth());
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 2:
                //Wrong answer
                System.out.println("Pressed 2");
                System.out.println("Player1 Health " + playerHealth + " Health3 width " + health3.getWidth());
                System.out.println("NPC1 Health " + npcHealth + " Health1 width " + health1.getWidth());
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 3:
                //Wrong answer
                System.out.println("Pressed 3");
                System.out.println("Player1 Health " + playerHealth + " Health3 width " + health3.getWidth());
                System.out.println("NPC1 Health " + npcHealth + " Health1 width " + health1.getWidth());
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 4:
                //Right answer
                System.out.println("Pressed 4");
                System.out.println("Player1 Health " + playerHealth + " Health3 width " + health3.getWidth());
                System.out.println("NPC1 Health " + npcHealth + " Health1 width " + health1.getWidth());
                npcHealth = health1.getWidth() - 140;
                health1.delete();
                health1 = new Rectangle(health1.getX(), health1.getY(), npcHealth, 10);
                health1.setColor(Color.RED);
                health1.fill();
                if (npcHealth <= 0) {
                    result = new Picture(190, 270, "win.png");
                    result.draw();
                    game.setQuestion2(true);
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
        }

    }

    public void question3() {
        System.out.println("Entered Question 3");
        System.out.println("Player Health " + playerHealth + " Health3 width " + health3.getWidth());
        System.out.println("NPC Health " + npcHealth+ " Health1 width " + health1.getWidth());

        if(rewind == 0) {
            rewind++;
            frame = new Picture(10, 10, "frame2.png");
            frame.draw();
            drawBar();
        }

        switch (game.getPressedKey()) {
            case 1:
                //Right answer
                System.out.println("Pressed 1");
                System.out.println("Player Health " + playerHealth + " Health3 width " + health3.getWidth());
                System.out.println("NPC Health " + npcHealth+ " Health1 width " + health1.getWidth());
                npcHealth = health1.getWidth() - 140;
                health1.delete();
                health1 = new Rectangle(health1.getX(), health1.getY(), npcHealth, 10);
                health1.setColor(Color.RED);
                health1.fill();
                if (npcHealth < 140) {
                    result = new Picture(190, 270, "next.png");
                    result.draw();
                    game.setQuestion3(true);
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    question4();
                                }
                            },
                            3000
                    );
                }
                break;
            case 2:
                //Wrong answer
                System.out.println("Pressed 2");
                System.out.println("Player Health " + playerHealth + " Health3 width " + health3.getWidth());
                System.out.println("NPC Health " + npcHealth+ " Health1 width " + health1.getWidth());
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 3:
                //Wrong answer
                System.out.println("Pressed 3");
                System.out.println("Player Health " + playerHealth + " Health3 width " + health3.getWidth());
                System.out.println("NPC Health " + npcHealth+ " Health1 width " + health1.getWidth());
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 4:
                //Wrong answer
                System.out.println("Pressed 4");
                System.out.println("Player Health " + playerHealth + " Health3 width " + health3.getWidth());
                System.out.println("NPC Health " + npcHealth+ " Health1 width " + health1.getWidth());
                playerHealth = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
            break;
        }

    }

    public void question4() {
        System.out.println("Entered Question 4");
        System.out.println("Player1 Health " + playerHealth1 + " Health3 width " + health3.getWidth());
        System.out.println("NPC1 Health " + npcHealth1 + " Health1 width " + health1.getWidth());

        if(rewind >= 1) {
            rewind++;
            frame = new Picture(10, 10, "frame2.png");
            frame.draw();
            drawBar();
        }

        switch (game.getPressedKey()) {
            case 1:
                //Wrong answer
                System.out.println("Pressed 1");
                System.out.println("Player1 Health " + playerHealth1 + " Health3 width " + health3.getWidth());
                System.out.println("NPC1 Health " + npcHealth1 + " Health1 width " + health1.getWidth());
                playerHealth1 = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth1, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 2:
                //Wrong answer
                System.out.println("Pressed 2");
                System.out.println("Player1 Health " + playerHealth1 + " Health3 width " + health3.getWidth());
                System.out.println("NPC1 Health " + npcHealth1 + " Health1 width " + health1.getWidth());
                playerHealth1 = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth1, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 3:
                //Right answer
                System.out.println("Pressed 3");
                System.out.println("Player1 Health " + playerHealth1 + " Health3 width " + health3.getWidth());
                System.out.println("NPC1 Health " + npcHealth1 + " Health1 width " + health1.getWidth());
                npcHealth1 = health1.getWidth() - 140;
                health1.delete();
                health1 = new Rectangle(health1.getX(), health1.getY(), npcHealth1, 10);
                health1.setColor(Color.RED);
                health1.fill();
                if (npcHealth <= 0) {
                    result = new Picture(190, 270, "win.png");
                    result.draw();
                    game.setQuestion4(true);
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
                break;
            case 4:
                //Wrong answer
                System.out.println("Pressed 3");
                System.out.println("Player1 Health " + playerHealth1 + " Health3 width " + health3.getWidth());
                System.out.println("NPC1 Health " + npcHealth1 + " Health1 width " + health1.getWidth());
                playerHealth1 = health3.getWidth() - 140;
                health3.delete();
                health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth1, 10);
                health3.setColor(Color.RED);
                health3.fill();
                if (playerHealth <= 0) {
                    result = new Picture(150, 270, "gameover.png");
                    result.draw();
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    result.delete();
                                    game.show();
                                }
                            },
                            3000
                    );
                }
            break;
        }

    }


    public void eraseCombat() {
        frame.delete();
        timer1.delete();
        timer2.delete();
        health1.delete();
        health2.delete();
        health3.delete();
        health4.delete();
    }

    public void timer() {

        while (timer1.getWidth() >= 0) {
            System.out.println(timer1.getWidth());
            int less = 100;
            int decrease = timer1.getWidth() - less;
            timer1.delete();
            timer1 = new Rectangle(timer1.getX(), timer1.getY(), decrease, timer1.getHeight());
            timer1.setColor(Color.CYAN);
            timer1.fill();

        }
    }

    public void drawBar(){
        //quiz time //width time and health
        timer1 = new Rectangle(timer1.getX(), timer1.getY(), timer1.getWidth(), timer1.getHeight());
        timer1.setColor(Color.CYAN);
        timer1.fill();
        timer2 = new Rectangle(timer2.getX(), timer2.getY(), timer2.getWidth(), timer2.getHeight());
        timer2.setColor(Color.BLACK);
        timer2.draw();

        //NPC health //width health
        health1 = new Rectangle(health1.getX(), health1.getY(), health1.getWidth(), health1.getHeight());
        health1.setColor(Color.RED);
        health1.fill();
        health2 = new Rectangle(health2.getX(), health2.getY(), health2.getWidth(), health2.getHeight());
        health2.setColor(Color.BLACK);
        health2.draw();

        //Player health //width health
        health3 = new Rectangle(health3.getX(), health3.getY(), health3.getWidth(), health3.getHeight());
        health3.setColor(Color.RED);
        health3.fill();
        health4 = new Rectangle(health4.getX(), health4.getY(), health4.getWidth(), health4.getHeight());
        health4.setColor(Color.BLACK);
        health4.draw();
    }
}


