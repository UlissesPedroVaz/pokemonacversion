package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.EasterEgg;
import lisboa.academiadecodigo.org.Map.Map;
import lisboa.academiadecodigo.org.Map.Menu;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.sound.sampled.*;

public class Game implements KeyboardHandler {


    // Propriedades
    private Player player;
    private EasterEgg easterEgg;
    private NPC npc;

    //private NPC npc2;
    private KeyboardControl keyboardControl;
    private Map map;
    private Combat combat;
    private Menu menu;

    private boolean keyPressed;
    private boolean menuOn;
    private boolean timer;
    private boolean question1, question2, question3, question4;

    private int pressedKey = 0;
    private int clickedTimes = 0;
    private int currentPlay;

    public boolean isQuestion1() {
        return question1;
    }

    public void setQuestion1(boolean question1) {
        this.question1 = question1;
    }

    public boolean isQuestion2() {
        return question2;
    }

    public void setQuestion2(boolean question2) {
        this.question2 = question2;
    }

    public boolean isQuestion3() {
        return question3;
    }

    public void setQuestion3(boolean question3) {
        this.question3 = question3;
    }

    public boolean isQuestion4() {
        return question4;
    }

    public void setQuestion4(boolean question4) {
        this.question4 = question4;
    }


    // Instancia os objectos
    public void init() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

        menu = new Menu();
        menuOn = true;

        keyboardControl = new KeyboardControl(this);
        //audioOn();
    }

    // Audio
    /*public void audioOn() throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        Scanner scanner = new Scanner(System.in);

        File file = new File("/Users/martimafonso/Desktop/ProjectGit/pokemonacversion/ProjectGame.v9/resources/audio/SevenNationArmy.wav");
        AudioInputStream audioStream = AudioSystem.getAudioInputStream(file);
        Clip clip = AudioSystem.getClip();
        clip.open(audioStream);

        String response = "";
        clip.start();
        response = scanner.next();

    }*/


    // Getter:
    //  -Get Clicled Times
    public int getClickedTimes() {
        return clickedTimes;
    }

    // +1 Clicked Time
    public void setOneClickedTime() {
        clickedTimes++;
    }

    // -1 Clicked Times
    public void lessOneClickedTime() {
        clickedTimes--;
    }

    public int getPressedKey() {
        return pressedKey;
    }

    public boolean isTimer() {
        return timer;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:

                if (menuOn) {
                    return;
                }
                // Confirma de esta em combate ou no easterEgg para nao se mexer:
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveUp();
                break;


            case KeyboardEvent.KEY_DOWN:

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveDown();
                break;

            case KeyboardEvent.KEY_LEFT:

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveLeft();
                break;

            case KeyboardEvent.KEY_RIGHT:

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveRight();
                break;

            case KeyboardEvent.KEY_Q:
                if (combat.isInCombat() || easterEgg.isInEasterEgg() && clickedTimes == 0) {
                    return;
                }
                clickedTimes = 0;
                player.endTalk();
                player.setTalking(false);

                break;


            case KeyboardEvent.KEY_SPACE:

                if (menuOn) {
                    return;
                }


                // Pedir easter EGG na posiação certa
                easterEgg.giveMeEgg();

                keyPressed = true;

                if ((keyPressed && player.getCol() == 2 && player.getRow() == 14 ||
                        player.getCol() == 2 && player.getRow() == 15 ||
                        player.getCol() == 2 && player.getRow() == 16)
                        ||
                        (keyPressed && player.getCol() == 28 && player.getRow() == 8 ||
                                player.getCol() == 28 && player.getRow() == 9 ||
                                player.getCol() == 28 && player.getRow() == 10)) {
                    System.out.println("Entrou no If");
                    if (clickedTimes == 0) {
                        System.out.println("Fez o talk");
                        player.startTalk();
                        clickedTimes++;
                        player.setTalking(true);
                        System.out.println("game is talking: " + player.isTalking());
                    } else {
                        System.out.println("Fez o talk 2");
                        // Resolver o problema de entrar no combate e nao apagar o player
                        clickedTimes = 0;
                        player.endTalk();
                        player.setTalking(false);


                        combat.startCombat();
                        player.setInCombat(true);


                        System.out.println("Key pressed.");
                        System.out.println("game is talking: " + player.isTalking());

                    }

                }


            case KeyboardEvent.KEY_ENTER:
                if (menuOn) {

                    System.out.println("Key pressed");
                    menu.erase();
                    map = new Map();

                    npc = new NPC(this, map);
                    player = new Player(this, map);
                    easterEgg = new EasterEgg(this, player);
                    combat = new Combat(this, map, player, npc);

                    menuOn = false;


                }
                break;

            case KeyboardEvent.KEY_1:
                System.out.println(" Pressed 1 and Boolean1234 " + isQuestion1() + " ," + isQuestion2() + " ," + isQuestion3() + " ," + isQuestion4());
                if (combat.isInCombat() && !isQuestion1()) {
                    System.out.println("1st if key 1 " + isQuestion1());
                    pressedKey = 1;
                    combat.question1();
                    System.out.println("1st if key 1 " + isQuestion1());

                } else if (!isQuestion2()) {
                    System.out.println("2nd if key 1 " + isQuestion2());
                    pressedKey = 1;
                    combat.question2();
                    System.out.println("2nd if key 1 " + isQuestion2());

                } else if (!isQuestion3()) {
                    System.out.println("3rd if key 1 " + isQuestion3());
                    pressedKey = 1;
                    combat.question3();
                    System.out.println("3rd if key 1 " + isQuestion3());

                } else if (!isQuestion4()) {
                    System.out.println("4th if key 1 " + isQuestion4());
                    pressedKey = 1;
                    combat.question4();
                    System.out.println("4th if key 1 " + isQuestion4());

                }
                pressedKey = 0;
                break;



            case KeyboardEvent.KEY_2:
                System.out.println(" Pressed 2 and Boolean1234 " + isQuestion1() + " ," + isQuestion2() + " ," + isQuestion3() + " ," + isQuestion4());
                if(combat.isInCombat() && !isQuestion1()){
                    System.out.println("1st if key 2 " + isQuestion1());
                    pressedKey = 2;
                    combat.question1();
                    System.out.println("1st if key 2 " + isQuestion1());

                } else if (!isQuestion2())  {
                    System.out.println("2nd if key 2 " + isQuestion2());
                    pressedKey = 2;
                    combat.question2();
                    System.out.println("2nd if key 2 " + isQuestion2());

                } else if (!isQuestion3()) {
                    System.out.println("3rd if key 2 " + isQuestion3());
                    pressedKey = 2;
                    combat.question3();
                    System.out.println("3rd if key 2 " + isQuestion3());

                } else if (!isQuestion4()) {
                    System.out.println("4th if key 2 " + isQuestion4());
                    pressedKey = 2;
                    combat.question4();
                    System.out.println("4th if key 2 " + isQuestion4());

                }
                pressedKey = 0;
                break;


            case KeyboardEvent.KEY_3:
                System.out.println(" Pressed 3 and Boolean1234 " + isQuestion1() + " ," + isQuestion2() + " ," + isQuestion3() + " ," + isQuestion4());
                if(combat.isInCombat() && !isQuestion1()){
                    System.out.println("1st if key 3 " + isQuestion1());
                    pressedKey = 3;
                    combat.question1();
                    System.out.println("1st if key 3 " + isQuestion1());

                } else if (!isQuestion2()) {
                    System.out.println("2nd if key 3 " + isQuestion1());
                    pressedKey = 3;
                    combat.question2();
                    System.out.println("2nd if key 3 " + isQuestion1());

                } else if (!isQuestion3()) {
                    System.out.println("3rd if key 3 " + isQuestion2());
                    pressedKey = 3;
                    combat.question3();
                    System.out.println("3rd if key 3 " + isQuestion2());

                } else if (!isQuestion4()) {
                    System.out.println("4th if key 3 " + isQuestion4());
                    pressedKey = 3;
                    combat.question4();
                    System.out.println("4th if key 3 " + isQuestion4());

                }
                pressedKey = 0;
                break;


            case KeyboardEvent.KEY_4:
                System.out.println(" Pressed 4 and Boolean1234 " + isQuestion1() + " ," + isQuestion2() + " ," + isQuestion3() + " ," + isQuestion4());
                if(combat.isInCombat() && !isQuestion1()){
                    System.out.println("1st if key 4 " + isQuestion1());
                    pressedKey = 4;
                    combat.question1();
                    System.out.println("1st if key 4 " + isQuestion1());
                    break;
                } else if (!isQuestion2()) {
                    System.out.println("2nd if key 4 " + isQuestion1());
                    pressedKey = 4;
                    combat.question2();
                    System.out.println("2nd if key 4 " + isQuestion1());
                    break;
                } else if (!isQuestion3()) {
                    System.out.println("3rd if key 4 " + isQuestion2());
                    pressedKey = 4;
                    combat.question3();
                    System.out.println("3rd if key 4 " + isQuestion2());
                    break;
                } else if (!isQuestion4()) {
                    System.out.println("4th if key 4 " + isQuestion4());
                    pressedKey = 4;
                    combat.question4();
                    System.out.println("4th if key 4 " + isQuestion4());
                    break;
                }
                pressedKey = 0;
                break;
        }
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                keyPressed = false;

        }
    }

    public void show(){
        map = new Map();

        npc = new NPC(this, map);
        player = new Player(this, map);
        easterEgg = new EasterEgg(this, player);
        combat = new Combat(this, map, player, npc);

        menuOn = false;
    }


}

