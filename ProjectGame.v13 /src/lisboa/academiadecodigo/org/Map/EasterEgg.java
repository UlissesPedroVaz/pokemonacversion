package lisboa.academiadecodigo.org.Map;

import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Game;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class EasterEgg {

    private Game game;
    private Player player;

    private Picture easterPicture;


    private Boolean inEasterEgg = false;

    public EasterEgg(Game game, Player player) {
        this.game = game;
        this.player = player;

    }

    // Getter de isInEasterEgg
    public boolean isInEasterEgg() {
        return inEasterEgg;
    }


    public void giveMeEgg() {


        if (player.getCol() != 22 && (player.getRow() != 19 || player.getRow() != 20)) {
            return;
        }

        if (game.getClickedTimes() == 0) {
            easterPicture = new Picture(400, 200, "mac.png");
            easterPicture.draw();
            game.setOneClickedTime();
            inEasterEgg = true;

        } else {
            easterPicture.delete();
            game.lessOneClickedTime();
            inEasterEgg = false;
            return;
        }
    }


}
