package lisboa.academiadecodigo.org.Characters;

import lisboa.academiadecodigo.org.Game;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {

    private String name;

    private Game game;
    private Map map;

    private boolean isTalking;


    private Picture playerMoveUp;
    private Picture playerMoveDown;
    private Picture playerMoveLeft;
    private Picture playerMoveRight;


    private int col;
    private int row;

    public Player(Game game, Map map) {
        this.game = game;
        this.map = map;


        playerMoveUp = new Picture(80, 20, "p1_up1.png");
        playerMoveDown = new Picture(80, 20, "p1_down1.png");
        playerMoveLeft = new Picture(80, 20, "p1_left1.png");
        playerMoveRight = new Picture(80, 20, "p1_right1.png");


        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());

        System.out.println("col: " + col + " Row: " + row);

    }


    public void moveUp() {

        playerMoveDown.delete();
        playerMoveLeft.delete();
        playerMoveRight.delete();
        playerMoveUp.draw();
        if (row == 0) {
            return;
        }
        if (col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16 && isTalking) {
            while (isTalking) {
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
        // Obstacles             -- Board--                      --Tables--
        if (row == 3 && (col < 29 && col > 11) || (row == 21 && (col < 21 && col > 8))) {
            return;
        }

        playerMoveUp.translate(0, -map.getCellSize());
        row--;

        // Other moves
        playerMoveDown.translate(0, -map.getCellSize());
        playerMoveLeft.translate(0, -map.getCellSize());
        playerMoveRight.translate(0, -map.getCellSize());


        System.out.println("Col: " + col + " Row: " + row);

    }

    public void moveDown() {

        playerMoveUp.delete();
        playerMoveLeft.delete();
        playerMoveRight.delete();
        playerMoveDown.draw();

        if (row == 25) {
            return;
        }
        if (col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16 && isTalking) {
            while (isTalking) {
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
        // Obstacles         -- Board--                               --Tables--
        if (row == 6 && (col < 28 && col > 8)) {
            return;
        }

        playerMoveDown.translate(0, map.getCellSize());
        row++;

        // Other moves
        playerMoveUp.translate(0, map.getCellSize());
        playerMoveLeft.translate(0, map.getCellSize());
        playerMoveRight.translate(0, map.getCellSize());
        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " + row);

    }

    public void moveLeft() {

        playerMoveUp.delete();
        playerMoveDown.delete();
        playerMoveRight.delete();
        playerMoveLeft.draw();
        if (col == -3) {
            return;
        }
        if (col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16 && isTalking) {
            while (isTalking) {
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
        // Obstacles    -- Board--                   --Tables--
        if ((col == 29 && row < 3) || (col == 30 && (row < 21 && row > 6))) {
            return;
        }

        playerMoveLeft.translate(-map.getCellSize(), 0);
        col--;

        // Other moves
        playerMoveUp.translate(-map.getCellSize(), 0);
        playerMoveDown.translate(-map.getCellSize(), 0);
        playerMoveRight.translate(-map.getCellSize(), 0);
        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " + row);
    }

    public void moveRight() {

        playerMoveUp.delete();
         playerMoveDown.delete();
         playerMoveLeft.delete();
        playerMoveRight.draw();
        if (col == 33) {
            return;
        }
        if (col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16 && isTalking) {
            while (isTalking) {
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
        // Obstacles   -- Board--                   --Tables--
        if ((col == 11 && row < 3) || (col == 7 && (row < 21 && row > 6))) {
            return;
        }

        playerMoveRight.translate(map.getCellSize(), 0);
        col++;

        // Other moves
        playerMoveUp.translate(map.getCellSize(), 0);
        playerMoveDown.translate(map.getCellSize(),0 );
        playerMoveLeft.translate(map.getCellSize(), 0);
        // System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " + row);
    }


    public void startTalk() {

        //position where near the npc where player can talk
        if (col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16) {
            map.dialog1();
            isTalking = true;
        }
    }

    public void endTalk() {

        //position where near the npc where player can talk
        if (col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16) {
            map.removedialog1();
            isTalking = false;
        }
    }


    public boolean isTalking() {
        return isTalking;
    }

    public void setTalking(boolean talking) {
        isTalking = talking;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getWidth() {
        return col * 20;
    }

    public int getHeight() {
        return row * 20;
    }
}

