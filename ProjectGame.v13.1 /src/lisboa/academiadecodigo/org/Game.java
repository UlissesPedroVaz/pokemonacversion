package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.EasterEgg;
import lisboa.academiadecodigo.org.Map.Map;
import lisboa.academiadecodigo.org.Map.Menu;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.sound.sampled.*;

public class Game implements KeyboardHandler {


    // Propriedades
    private Player player;
    private EasterEgg easterEgg;
    private NPC npc;

    //private NPC npc2;
    private KeyboardControl keyboardControl;
    private Map map;
    private Combat combat;
    private Menu menu;

    // Booleanos
    private boolean keyPressed;
    private boolean menuOn = true;
    private boolean timer;
    private boolean question1, question2, question3, question4;
    private boolean newFrame;
    private boolean newFrame1;

    private int pressedKey = 0;
    private int clickedTimes = 0;
    private int currentPlay;

    // Instancia os objectos
    public void init() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

        menu = new Menu(this);
        menuOn = true;

        keyboardControl = new KeyboardControl(this);
        //audioOn();
    }

    // Audio
  /*  public void audioOn() throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        Scanner scanner = new Scanner(System.in);

        File file = new File("resources/SevenNationArmy.wav");
        AudioInputStream audioStream = AudioSystem.getAudioInputStream(file);
        Clip clip = AudioSystem.getClip();
        clip.open(audioStream);

        String response = "";
        clip.start();
        response = scanner.next();

    } */

// Getters e Setters para as Question
    public boolean isQuestion1() {
        return question1;
    }

    public void setQuestion1(boolean question1) {
        this.question1 = question1;
    }

    public boolean isQuestion2() {
        return question2;
    }

    public void setQuestion2(boolean question2) {
        this.question2 = question2;
    }

    public boolean isQuestion3() {
        return question3;
    }

    public void setQuestion3(boolean question3) {
        this.question3 = question3;
    }

    public boolean isQuestion4() {
        return question4;
    }

    public void setQuestion4(boolean question4) {
        this.question4 = question4;
    }


    // Getter:
    //  -Get Clicled Times
    public int getClickedTimes() {
        return clickedTimes;
    }

    public int getPressedKey() {
        return pressedKey;
    }

    // +1 Clicked Time
    public void setOneClickedTime() {
        clickedTimes++;
    }

    // -1 Clicked Times
    public void lessOneClickedTime() {
        clickedTimes--;
    }

    // Set menuOn (Booleano)
    public void setMenuOff(){
        menuOn = false;
    }

    public boolean isNewFrame() {
        return newFrame;
    }

    public boolean isTimer() {
        return timer;
    }

    public void setNewFrame(boolean newFrame) {
        this.newFrame = newFrame;
    }

    public boolean isNewFrame1() {
        return newFrame1;
    }

    public void setNewFrame1(boolean newFrame1) {
        this.newFrame1 = newFrame1;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:

                if (menuOn) {
                    return;
                }
                // Confirma de esta em combate ou no easterEgg para nao se mexer:
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    System.out.println("esta a bater no isInCombat");
                    return;
                }
                player.moveUp();
                break;


            case KeyboardEvent.KEY_DOWN:

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveDown();
                break;

            case KeyboardEvent.KEY_LEFT:

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveLeft();
                break;

            case KeyboardEvent.KEY_RIGHT:
                System.out.println(" Menu on: " + menuOn);
                System.out.println(" isTalking: " + player.isTalking());
                System.out.println(" Clicked Times: " + clickedTimes);
                System.out.println(" inCombat: " + combat.isInCombat());

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveRight();
                break;

                // Tecla Q de Quit:
            case KeyboardEvent.KEY_Q:
                if (menuOn) {
                    return;
                }
                System.out.println("Pre - Key Q, isInCombat: " + combat.isInCombat());
                System.out.println("Pre - Key Q, isTalking: " + player.isTalking());
                System.out.println("Pre - Key Q, Clicked Times: " + clickedTimes);
                if (combat.isInCombat() || easterEgg.isInEasterEgg() || clickedTimes == 0) {
                    return;
                }
                System.out.println("Entrou no reset do Q");

                player.endTalk(clickedTimes);
                clickedTimes = 0;
                player.setTalking(false);

                System.out.println(" Key Q, isTalking: " + player.isTalking());
                System.out.println(" Key Q, Clicked Times: " + clickedTimes);
                break;


            case KeyboardEvent.KEY_SPACE:
                // Se estiver no menu
                if (menuOn) {
                    return;
                }

                System.out.println("\n Pre - Key Space: isInCombat: " + combat.isInCombat());
                System.out.println(" Pre - Key Space: PlayerIsTalking: " + player.isTalking());
                System.out.println("Pre - Key Space: Clicked Times: " + clickedTimes);
                // Pedir easter EGG na posiação certa
                easterEgg.giveMeEgg();

                keyPressed = true;

                if ((keyPressed && player.getCol() == 2 && player.getRow() == 14 ||
                        player.getCol() == 2 && player.getRow() == 15 ||
                        player.getCol() == 2 && player.getRow() == 16)
                        ||
                        (keyPressed && player.getCol() == 28 && player.getRow() == 8 ||
                                player.getCol() == 28 && player.getRow() == 9 ||
                                player.getCol() == 28 && player.getRow() == 10)) {

                    if (clickedTimes == 0) {
                        player.setTalking(true);
                        player.startTalk();
                        clickedTimes++;

                        System.out.println("1st Key Space: isInCombat: " + combat.isInCombat());
                        System.out.println("1st Key Space: PlayerIsTalking: " + player.isTalking());
                        System.out.println("1st Key Space: Clicked Times: " + clickedTimes);

                    } else if (clickedTimes == 1) {
                        System.out.println("2nd Key Space (Player talk): isInCombat: " + combat.isInCombat());
                        System.out.println("2nd Key Space (Player talk): PlayerIsTalking: " + player.isTalking());
                        System.out.println("2nd Key Space: Clicked Times: " + clickedTimes);

                        player.startTalk();
                        clickedTimes++;
                    } else if (clickedTimes == 2) {
                        System.out.println("3st Key Space: isInCombat: " + combat.isInCombat());
                        System.out.println("3st Key Space: PlayerIsTalking: " + player.isTalking());
                        System.out.println("3rd Key Space: Clicked Times: " + clickedTimes);

                        player.startTalk();
                        clickedTimes++;


                    } else if (clickedTimes == 3) {
                        // Acaba o dialogo
                        clickedTimes = 0;
                        player.endTalk(clickedTimes);
                        player.setTalking(false);
                        // Comeca o combate
                        System.out.println("Entrou antes de fazer startCombat");
                        combat.startCombat();
                        player.setInCombat(true);


                        System.out.println("4rd Key Space: Clicked Times: " + clickedTimes);

                        System.out.println("4rd Key Space: isInCombat: " + combat.isInCombat());
                        System.out.println("4rd Key Space: PlayerIsTalking: " + player.isTalking());
                    }
                }


            case KeyboardEvent.KEY_ENTER:
                if (menuOn) {

                    System.out.println("Key pressed");
                    menu.erase();
                    map = new Map();

                    npc = new NPC(this, map);
                    player = new Player(this, map);
                    easterEgg = new EasterEgg(this, player);
                    combat = new Combat(this, map, player, npc);

                    // Call Loading
                    menu.loading();

                }
                break;

            case KeyboardEvent.KEY_1:
                if (menuOn) {
                    return;
                }
                System.out.println(" Pressed 1 and Boolean1234 " + isQuestion1() + " ," + isQuestion2() + " ," + isQuestion3() + " ," + isQuestion4());
                if (combat.isInCombat() && !isQuestion1()) {
                    System.out.println("1st if key 1 " + isQuestion1());
                    pressedKey = 1;
                    combat.question1();
                    System.out.println("1st if key 1 " + isQuestion1());
                    setQuestion1(true);
                } else if (!isQuestion2()) {
                    System.out.println("2nd if key 1 " + isQuestion2());
                    pressedKey = 1;
                    combat.question2();
                    System.out.println("2nd if key 1 " + isQuestion2());
                    setQuestion2(true);
                } else if (!isQuestion3()) {
                    System.out.println("3rd if key 1 " + isQuestion3());
                    pressedKey = 1;
                    combat.question3();
                    System.out.println("3rd if key 1 " + isQuestion3());
                    setQuestion3(true);
                } else if (!isQuestion4()) {
                    System.out.println("4th if key 1 " + isQuestion4());
                    pressedKey = 1;
                    combat.question4();
                    System.out.println("4th if key 1 " + isQuestion4());
                    setQuestion4(true);
                }
                pressedKey = 0;
                break;


            case KeyboardEvent.KEY_2:
                if (menuOn) {
                    return;
                }
                System.out.println(" Pressed 2 and Boolean1234 " + isQuestion1() + " ," + isQuestion2() + " ," + isQuestion3() + " ," + isQuestion4());
                if(combat.isInCombat() && !isQuestion1()){
                    System.out.println("1st if key 2 " + isQuestion1());
                    pressedKey = 2;
                    combat.question1();
                    System.out.println("1st if key 2 " + isQuestion1());
                    setQuestion1(true);
                } else if (!isQuestion2())  {
                    System.out.println("2nd if key 2 " + isQuestion2());
                    pressedKey = 2;
                    combat.question2();
                    System.out.println("2nd if key 2 " + isQuestion2());

                } else if (!isQuestion3()) {
                    System.out.println("3rd if key 2 " + isQuestion3());
                    pressedKey = 2;
                    combat.question3();
                    System.out.println("3rd if key 2 " + isQuestion3());

                } else if (!isQuestion4()) {
                    System.out.println("4th if key 2 " + isQuestion4());
                    pressedKey = 2;
                    combat.question4();
                    System.out.println("4th if key 2 " + isQuestion4());

                }
                pressedKey = 0;
                break;


            case KeyboardEvent.KEY_3:
                if (menuOn) {
                    return;
                }
                System.out.println(" Pressed 3 and Boolean1234 " + isQuestion1() + " ," + isQuestion2() + " ," + isQuestion3() + " ," + isQuestion4());
                if(combat.isInCombat() && !isQuestion1()){
                    System.out.println("1st if key 3 " + isQuestion1());
                    pressedKey = 3;
                    combat.question1();
                    System.out.println("1st if key 3 " + isQuestion1());

                } else if (!isQuestion2()) {
                    System.out.println("2nd if key 3 " + isQuestion1());
                    pressedKey = 3;
                    combat.question2();
                    System.out.println("2nd if key 3 " + isQuestion1());

                } else if (!isQuestion3()) {
                    System.out.println("3rd if key 3 " + isQuestion2());
                    pressedKey = 3;
                    combat.question3();
                    System.out.println("3rd if key 3 " + isQuestion2());

                } else if (!isQuestion4()) {
                    System.out.println("4th if key 3 " + isQuestion4());
                    pressedKey = 3;
                    combat.question4();
                    System.out.println("4th if key 3 " + isQuestion4());

                }
                pressedKey = 0;
                break;

            case KeyboardEvent.KEY_4:
                if (menuOn) {
                    return;
                }
                System.out.println(" Pressed 4 and Boolean1234 " + isQuestion1() + " ," + isQuestion2() + " ," + isQuestion3() + " ," + isQuestion4());
                if(combat.isInCombat() && !isQuestion1()){
                    System.out.println("1st if key 4 " + isQuestion1());
                    pressedKey = 4;
                    combat.question1();
                    System.out.println("1st if key 4 " + isQuestion1());

                } else if (!isQuestion2()) {
                    System.out.println("2nd if key 4 " + isQuestion1());
                    pressedKey = 4;
                    combat.question2();
                    System.out.println("2nd if key 4 " + isQuestion1());

                } else if (!isQuestion3()) {
                    System.out.println("3rd if key 4 " + isQuestion2());
                    pressedKey = 4;
                    combat.question3();
                    System.out.println("3rd if key 4 " + isQuestion2());

                } else if (!isQuestion4()) {
                    System.out.println("4th if key 4 " + isQuestion4());
                    pressedKey = 4;
                    combat.question4();
                    System.out.println("4th if key 4 " + isQuestion4());

                }
                pressedKey = 0;
                break;

        }
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                keyPressed = false;

        }
    }

    public void showALL() {
        map.show();
        npc.showNPC();
        player.showPlayerDown();

    }


}


