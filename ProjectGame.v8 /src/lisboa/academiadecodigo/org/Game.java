package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.EasterEgg;
import lisboa.academiadecodigo.org.Map.Map;
import lisboa.academiadecodigo.org.Map.Menu;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Game implements KeyboardHandler {


    // Propriedades
    private Player player;
    private NPC npc;

    private EasterEgg easterEgg;

    private Menu menu;
    private Map map;
    private Combat combat;


    private KeyboardControl keybordcontrol;

    private boolean menuOn;

    private boolean keypressed;


    private int clickedTimes = 0;


    // Instancia os objectos
    public void init() {

        menu = new Menu();
        menuOn = true;

        keybordcontrol = new KeyboardControl(this);

    }



    // Getter:
    //  -Get Clicled Times
    public int getClickedTimes() {
        return clickedTimes;
    }

    // +1 Clicked Time
    public void setOneClickedTime() {
        clickedTimes++;
    }

    // -1 Clicked Times
    public void lessOneClickedTime() {
        clickedTimes--;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:

                if (menuOn){
                    return;
                }
                // Confirma de esta em combate ou no easterEgg para nao se mexer:
                if(combat.isInCombat() || easterEgg.isInEasterEgg()){
                    return;
                }
                player.moveUp();
                break;

            case KeyboardEvent.KEY_DOWN:
                if (menuOn){
                    return;
                }
                if(combat.isInCombat() || easterEgg.isInEasterEgg()){
                    return;
                }
                player.moveDown();
                break;

            case KeyboardEvent.KEY_LEFT:
                if (menuOn){
                    return;
                }
                if(combat.isInCombat() || easterEgg.isInEasterEgg()){
                    return;
                }
                player.moveLeft();
                break;

            case KeyboardEvent.KEY_RIGHT:
                if (menuOn){
                    return;
                }
                if(combat.isInCombat() || easterEgg.isInEasterEgg()){
                    return;
                }
                player.moveRight();
                break;


            case KeyboardEvent.KEY_SPACE:

                if (menuOn){
                    return;
                }

                // Pedir easter EGG na posiação certa
                easterEgg.giveMeEgg();

                keypressed = true;


                if ((keypressed && player.getCol() == 2 && player.getRow() == 14 ||
                        player.getCol() == 2 && player.getRow() == 15 ||
                        player.getCol() == 2 && player.getRow() == 16)
                        ||
                        (keypressed && player.getCol() == 28 && player.getRow() == 8 ||
                                player.getCol() == 28 && player.getRow() == 9 ||
                                player.getCol() == 28 && player.getRow() == 10)) {

                    if (clickedTimes == 0) {
                        player.startTalk();
                        clickedTimes++;
                        player.setTalking(true);
                        System.out.println("game is talking: " + player.isTalking());
                    } else {

                        // Resolver o problema de entrar no combate e nao apagar o player
                        clickedTimes = 0;
                        player.endTalk();
                        player.setTalking(false);

                       combat.startCombat();
                       player.setInCombat(true);
                        System.out.println("Key pressed.");
                        System.out.println("game is talking: " + player.isTalking());

                    }
                }
                break;

                    case KeyboardEvent.KEY_ENTER:
                        menu.erase();
                        map = new Map();

                        npc = new NPC(this, map);
                        player = new Player(this, map);
                        easterEgg = new EasterEgg(this, player);
                        combat = new Combat(this, map, player, npc);

                        menuOn = false;
                        break;




           

           /* case KeyboardEvent.KEY_Y:
                map.erase();
                combat.startCombat();
                System.out.println("Key pressed.");
                break;
*/
        }
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                keypressed = false;

        }
    }


}
