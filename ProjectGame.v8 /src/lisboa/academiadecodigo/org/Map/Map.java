package lisboa.academiadecodigo.org.Map;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.awt.*;

public class Map {

    private Rectangle background;

    private Picture map1, dialog1, dialog2;

    private Text text1, text2;

    public static final int COLS = 8;
    public static final int ROWS = 6;

    public static final int CELL_SIZE = 20;
    public static final int PADDING = 10;

    // Construtor
    public Map() {

        background = new Rectangle(PADDING, PADDING, getWidth(), getHeight());
        map1 = new Picture(PADDING,PADDING,"Background.v2.png");


        background.fill();
        map1.draw();

    }

    public void dialog1(){
        dialog1 = new Picture(210,220,"text.png");
        dialog1.draw();
        dialog1.grow(30,0);

        text1 = new Text(230,240,"Hello, are ready to start this Adventure?");
        text2 = new Text(230,260,"Yes >(Press - Space)  Quit >(Press - Q)");
        text1.draw();
        text2.draw();
        text1.grow(5,5);
        text2.grow(5,5);
    }

    public void dialog2(){
        dialog2 = new Picture(340,120,"text2.png");
        dialog2.draw();
        dialog2.grow(30,0);

        text1 = new Text(335,140,"Hello, are ready to start this Adventure?");
        text2 = new Text(335,160,"Yes >(Press - Space)  Quit >(Press - Q)");
        text1.draw();
        text2.draw();
        text1.grow(5,5);
        text2.grow(5,5);
    }

    public void removedialog1(){
        dialog1.delete();
        text1.delete();
        text2.delete();
    }

    public void removedialog2(){

        dialog2.delete();
        text1.delete();
        text2.delete();
    }

    // Apaga o BackGround
    public void erase(){
        map1.delete();
    }

    // Desenha o BackGround
    public void show(){
        map1.draw();
    }


    //GET COL
    public int getCols() {
        return COLS;
    }

    public int getRows() {
        return ROWS;
    }

    // Get width
    public int getWidth() {
        return COLS * CELL_SIZE;
    }

    // Get Height
    public int getHeight() {
        return ROWS * CELL_SIZE;
    }

    // get Cell-size
    public int getCellSize() {
        return CELL_SIZE;

    }



}
