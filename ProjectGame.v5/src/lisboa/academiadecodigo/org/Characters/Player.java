package lisboa.academiadecodigo.org.Characters;

import lisboa.academiadecodigo.org.Game;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {

    private String name;

    private Game game;
    private Map map;

    private boolean isTalking;
    private boolean keypressed;

    //private int clickedTimes = 0;

    private Picture playerMove1;
    private Picture playerMove2;
    private Picture playerMove3;
    private Picture playerMove4;

    private int col;
    private int row;

    public Player(Game game, Map map) {
        this.game = game;
        this.map = map;


        playerMove1 = new Picture(720,540, "p1_up1.png");
        playerMove2 = new Picture(720,540, "p1_down1.png");
        playerMove3 = new Picture(720,540, "p1_left1.png");
        playerMove4 = new Picture(720,540, "p1_right1.png");


        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());

        System.out.println("Col: " + col + " Row: " +row);

    }


    public void moveUp() {
        // playerMove2.delete();
        // playerMove3.delete();
        // playerMove4.delete();
        playerMove1.draw();
        int currentCol = col;
        int currentRow = row;
        System.out.println("Col1: " + col + " Row1: " +row);

        // limit of the up side of map
        if (row == 26){
            return;
        }
        //position where near the npc where player can talk
        if (col == 31 && row == 4 ||
                col == 31 && row == 5 ||
                col == 31 && row == 6) {
            while (isTalking){
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
            System.out.println("player2 is talking: " + isTalking);
            playerMove1.translate(0, -map.getCellSize());
            row++;

            System.out.println("Col: " + col + " Row: " +row);


    }

    public void moveDown() {
        // playerMove1.delete();
        // playerMove3.delete();
        // playerMove4.delete();
        playerMove1.draw();

        // limit of the down side of map
        if (row == 0){
            return;
        }
        //position where near the npc where player can talk
        if (col == 31 && row == 4 ||
                col == 31 && row == 5 ||
                col == 31 && row == 6 && isTalking) {
            while (isTalking){
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
            System.out.println("player2 is talking: " + isTalking);
            playerMove1.translate(0, map.getCellSize());
            row--;

            System.out.println("Col: " + col + " Row: " +row);

    }



    public void moveLeft() {
        // playerMove1.delete();
        // playerMove2.delete();
        // playerMove4.delete();
        playerMove1.draw();

        // limit of the left side of map
        if (col == 35) {
            return;
        }
        //position where near the npc where player can talk
        if (col == 31 && row == 4 ||
                    col == 31 && row == 5 ||
                    col == 31 && row == 6 && isTalking){
            while (isTalking){
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
            System.out.println("player2 is talking: " + isTalking);
            playerMove1.translate(-map.getCellSize(), 0);
            col++;

            System.out.println("Col: " + col + " Row: " +row);


    }



    public void moveRight() {
        // playerMove1.delete();
        // playerMove2.delete();
        // playerMove3.delete();
        playerMove1.draw();

        // limit of the right side of map
        if (col == -1) {
            return;
        }
        //position where near the npc where player can talk
        if (col == 31 && row == 4 ||
                    col == 31 && row == 5 ||
                    col == 31 && row == 6 && isTalking) {
            while (isTalking){
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
            System.out.println("player2 is talking: " + isTalking);
            playerMove1.translate(map.getCellSize(), 0);
            col--;

            System.out.println("Col: " + col + " Row: " +row);

    }



    public void startTalk() {

        //position where near the npc where player can talk
        if (col == 31 && row == 4 ||
                col == 31 && row == 5 ||
                col == 31 && row == 6) {
                map.dialog1();
                isTalking = true;
        }
    }

    public void endTalk() {

        //position where near the npc where player can talk
        if (col == 31 && row == 4 ||
                col == 31 && row == 5 ||
                col == 31 && row == 6) {
                map.removedialog1();
                isTalking = false;
            }
        }


    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public boolean isTalking() {
        return isTalking;
    }

    public void setTalking(boolean talking) {
        isTalking = talking;
    }
}




