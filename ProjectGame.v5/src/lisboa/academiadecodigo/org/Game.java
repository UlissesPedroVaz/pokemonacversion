package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Game implements KeyboardHandler {


    // Propriedades


    private Player player;
    private NPC npc;
    private KeyboardControl keybordcontrol;
    private Map map;

    private boolean keypressed;

    private int clickedTimes = 0;


    // Instancia os objectos
    public void init() {

        map = new Map();
        npc = new NPC(this, map);
        player = new Player(this, map);


        keybordcontrol = new KeyboardControl(this);
    }



    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_UP:
                player.moveUp();
                break;

            case KeyboardEvent.KEY_DOWN:
                player.moveDown();
                break;

            case KeyboardEvent.KEY_LEFT:
                player.moveLeft();
                break;

            case KeyboardEvent.KEY_RIGHT:
                player.moveRight();
                break;

            case KeyboardEvent.KEY_SPACE:
               keypressed = true;;

                    if (keypressed && player.getCol() == 31 && player.getRow() == 4 ||
                                      player.getCol() == 31 && player.getRow() == 5 ||
                                      player.getCol() == 31 && player.getRow() == 6 ) {

                        if (clickedTimes == 0){
                            player.startTalk();
                            clickedTimes++;
                            player.setTalking(true);
                            System.out.println("game is talking: " + player.isTalking());
                        }else{
                        player.endTalk();
                        clickedTimes = 0;
                        player.setTalking(false);
                            System.out.println("game is talking: " + player.isTalking());
                        }
                    }

                break;

            case KeyboardEvent.KEY_Q:
                break;
            case KeyboardEvent.KEY_W:
                break;
            case KeyboardEvent.KEY_E:
                break;
            default:
                System.out.println("Something went wrong!");
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                keypressed = false;
        }
    }


}
