package lisboa.academiadecodigo.org.Characters;

import lisboa.academiadecodigo.org.Game;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class NPC {

    private String name;

    private Game game;
    private Map map;


    private Picture npc1;


    private int col = 0;
    private int row = 0;

    public NPC(Game game, Map map) {
        this.game = game;
        this.map = map;

        npc1 = new Picture(150,450, "p1_down1.png");
        System.out.println(" NPC :    Col: " + npc1.getX() + " Row: " + npc1.getY());

        npc1.draw();
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

}
