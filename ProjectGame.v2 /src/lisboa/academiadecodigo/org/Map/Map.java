package lisboa.academiadecodigo.org.Map;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Map {

    private Rectangle background;

    private Picture map1;

    public static final int COLS = 80;
    public static final int ROWS = 60;

    private int width;
    private int height;

    public static final int CELL_SIZE = 10;
    public static final int PADDING = 10;

    // Construtor
    public Map() {

        width = COLS * CELL_SIZE;
        height = ROWS * CELL_SIZE;

        background = new Rectangle(PADDING, PADDING, width, height);
        map1 = new Picture(PADDING,PADDING,"background.png");

        background.fill();
        map1.draw();
    }

    //GET COL
    public int getCols() {
        return COLS;
    }

    public int getRows() {
        return ROWS;
    }

    // Get width
    public int getWidth() {
        return width;
    }

    // Get Height
    public int getHeight() {
        return height;
    }

    // get Cell-size
    public int getCellSize() {
        return CELL_SIZE;

    }



}
