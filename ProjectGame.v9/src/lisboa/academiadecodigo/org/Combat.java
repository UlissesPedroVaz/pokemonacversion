package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Combat {

    private String name;
    private int health; //pride
    private boolean inCombat;

    private Rectangle timer1, timer2, health1, health2;
    private Picture frame, background;
    private Text quiz, answer;

    private Map map;
    private Game game;

    private Player player;
    private NPC npc;


    public Combat(Game game, Map map, Player player, NPC npc) {
        this.game = game;
        this.map = map;
        this.player = player;
        this.npc = npc;
    }

    // Acrescentado:
    // Getter inCombat:
    public boolean isInCombat() {
        return inCombat;
    }


    public void startCombat() {

        // Apaga o mapa
        map.erase();
        System.out.println("Fez erase do mapa");

        // Apaga o Player
        player.erasePalyer();
        System.out.println("Fez erase do player");


        // Apaga os NPCS
        npc.eraseNPC();




        if (!inCombat) {
            System.out.println("Entrou no while Combat");
            inCombat = true;

            //quiz frames
            frame = new Picture(10, 10, "frame.png");
            frame.draw();

            //quiz time //width time and health
            timer1 = new Rectangle(30, 170, 278, 10);
            timer1.setColor(Color.CYAN);
            timer1.fill();
            timer2 = new Rectangle(29, 169, 279, 11);
            timer2.setColor(Color.BLACK);
            timer2.draw();

            //NPC health //width health
            health1 = new Rectangle(30, 180, 278, 10);
            health1.setColor(Color.RED);
            health1.fill();
            health1 = new Rectangle(29, 179, 279, 11);
            health1.setColor(Color.BLACK);
            health1.draw();

            //Player health //width health
            health2 = new Rectangle(511, 440, 278, 10);
            health2.setColor(Color.RED);
            health2.fill();
            health2 = new Rectangle(510, 439, 279, 11);
            health2.setColor(Color.BLACK);
            health2.draw();

            //Text quiz
            quiz = new Text(60, 120, "text is in the right position");
            quiz.draw();

            //Answers
            answer = new Text(550, 500, "Hello this is a test, to see if the");
            answer.draw();


        }


    }



    public void eraseCombat() {
        background.delete();
    }


}
