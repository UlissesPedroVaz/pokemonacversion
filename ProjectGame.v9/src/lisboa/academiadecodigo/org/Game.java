package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.EasterEgg;
import lisboa.academiadecodigo.org.Map.Map;
import lisboa.academiadecodigo.org.Map.Menu;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.sound.sampled.*;

public class Game implements KeyboardHandler {


    // Propriedades
    private Player player;
    private EasterEgg easterEgg;
    private NPC npc;

    //private NPC npc2;
    private KeyboardControl keybordcontrol;
    private Map map;
    private Combat combat;
    private Menu menu;

    private boolean keypressed;
    private boolean menuOn;


    private int clickedTimes = 0;


    // Instancia os objectos
    public void init() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

        menu = new Menu();
        menuOn = true;

        keybordcontrol = new KeyboardControl(this);
        audioOn();
    }

    // Audio
    public void audioOn() throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        Scanner scanner = new Scanner(System.in);

        File file = new File("/Users/martimafonso/Desktop/ProjectGit/pokemonacversion/ProjectGame.v9/resources/audio/SevenNationArmy.wav");
        AudioInputStream audioStream = AudioSystem.getAudioInputStream(file);
        Clip clip = AudioSystem.getClip();
        clip.open(audioStream);

        String response = "";
        clip.start();
        response = scanner.next();

    }


    // Getter:
    //  -Get Clicled Times
    public int getClickedTimes() {
        return clickedTimes;
    }

    // +1 Clicked Time
    public void setOneClickedTime() {
        clickedTimes++;
    }

    // -1 Clicked Times
    public void lessOneClickedTime() {
        clickedTimes--;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:

                if (menuOn) {
                    return;
                }
                // Confirma de esta em combate ou no easterEgg para nao se mexer:
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveUp();
                break;


            case KeyboardEvent.KEY_DOWN:

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveDown();
                break;

            case KeyboardEvent.KEY_LEFT:

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveLeft();
                break;

            case KeyboardEvent.KEY_RIGHT:

                if (menuOn) {
                    return;
                }
                if (combat.isInCombat() || easterEgg.isInEasterEgg()) {
                    return;
                }
                player.moveRight();
                break;

            case KeyboardEvent.KEY_Q:
                if (combat.isInCombat() || easterEgg.isInEasterEgg() && clickedTimes == 0) {
                    return;
                }
                clickedTimes = 0;
                player.endTalk();
                player.setTalking(false);

                break;


            case KeyboardEvent.KEY_SPACE:

                if (menuOn) {
                    return;
                }


                // Pedir easter EGG na posiação certa
                easterEgg.giveMeEgg();

                keypressed = true;

                if ((keypressed && player.getCol() == 2 && player.getRow() == 14 ||
                        player.getCol() == 2 && player.getRow() == 15 ||
                        player.getCol() == 2 && player.getRow() == 16)
                        ||
                        (keypressed && player.getCol() == 28 && player.getRow() == 8 ||
                                player.getCol() == 28 && player.getRow() == 9 ||
                                player.getCol() == 28 && player.getRow() == 10)) {
                    System.out.println("Entrou no If");
                    if (clickedTimes == 0) {
                        System.out.println("Fez o talk");
                        player.startTalk();
                        clickedTimes++;
                        player.setTalking(true);
                        System.out.println("game is talking: " + player.isTalking());
                    } else {
                        System.out.println("Fez o talk 2");
                        // Resolver o problema de entrar no combate e nao apagar o player
                        clickedTimes = 0;
                        player.endTalk();
                        player.setTalking(false);

                        combat.startCombat();
                        player.setInCombat(true);


                        System.out.println("Key pressed.");
                        System.out.println("game is talking: " + player.isTalking());

                    }

                }


            case KeyboardEvent.KEY_ENTER:
                if (menuOn) {

                    menu.erase();
                    map = new Map();

                    npc = new NPC(this, map);
                    player = new Player(this, map);
                    easterEgg = new EasterEgg(this, player);
                    combat = new Combat(this, map, player, npc);

                    menuOn = false;


                }
                break;
        }
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                keypressed = false;

        }
    }


}

