package lisboa.academiadecodigo.org.Characters;

import lisboa.academiadecodigo.org.Game;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {

    private String name;

    private Game game;
    private Map map;


    private Picture playerMove1;
    private Picture playerMove2;
    private Picture playerMove3;
    private Picture playerMove4;

    private int col;
    private int row;

    public Player(Game game, Map map) {
        this.game = game;
        this.map = map;


        playerMove1 = new Picture(720,540, "p1_up1.png");
        playerMove2 = new Picture(720,540, "p1_down1.png");
        playerMove3 = new Picture(720,540, "p1_left1.png");
        playerMove4 = new Picture(720,540, "p1_right1.png");


        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());

        System.out.println("Col: " + col + " Row: " +row);

    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }


    public void moveUp() {
        // playerMove2.delete();
        // playerMove3.delete();
        // playerMove4.delete();
        playerMove1.draw();


        playerMove1.translate(0, -map.getCellSize());
        row++;
        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " +row);

    }

    public void moveDown() {
        // playerMove1.delete();
        // playerMove3.delete();
        // playerMove4.delete();
        playerMove1.draw();


        playerMove1.translate(0, map.getCellSize());
        row--;
        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " +row);

    }

    public void moveLeft() {
        // playerMove1.delete();
        // playerMove2.delete();
        // playerMove4.delete();
        playerMove1.draw();


        playerMove1.translate(-map.getCellSize(), 0);
        col++;
        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " +row);
    }

    public void moveRight() {
        // playerMove1.delete();
        // playerMove2.delete();
        // playerMove3.delete();
        playerMove1.draw();


        playerMove1.translate(map.getCellSize(), 0);
        col--;
        // System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " +row);
    }



}

