package lisboa.academiadecodigo.org.Map;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.awt.*;

public class Map {

    private Rectangle background;

    private Picture map1;
    private Picture dialog1;

    public static final int COLS = 8;
    public static final int ROWS = 6;

    private int width;
    private int height;

    public static final int CELL_SIZE = 20;
    public static final int PADDING = 10;

    // Construtor
    public Map() {

        width = COLS * CELL_SIZE;
        height = ROWS * CELL_SIZE;

        background = new Rectangle(PADDING, PADDING, width, height);
        map1 = new Picture(PADDING,PADDING,"background.png");

        background.fill();
        map1.draw();
    }

    public void dialog1(){
        dialog1 = new Picture(150,340,"text1.png");
        dialog1.draw();

        Text text1 = new Text(175,360,"Hello this is a test, to see if the");
        Text text2 = new Text(175,380,"text is in the right position");
        text1.draw();
        text2.draw();
        text1.grow(10,10);
        text2.grow(10,10);
    }

    //GET COL
    public int getCols() {
        return COLS;
    }

    public int getRows() {
        return ROWS;
    }

    // Get width
    public int getWidth() {
        return width;
    }

    // Get Height
    public int getHeight() {
        return height;
    }

    // get Cell-size
    public int getCellSize() {
        return CELL_SIZE;

    }



}
