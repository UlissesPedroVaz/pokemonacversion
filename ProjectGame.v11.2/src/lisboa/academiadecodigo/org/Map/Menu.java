package lisboa.academiadecodigo.org.Map;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Menu {

    private Picture menu;

    public Menu() {
        menu = new Picture(Map.PADDING, Map.PADDING, "menu.png");
        menu.draw();

    }

    public void erase () {
        menu.delete();
    }
}
