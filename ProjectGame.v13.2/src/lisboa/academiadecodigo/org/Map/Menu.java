package lisboa.academiadecodigo.org.Map;

import lisboa.academiadecodigo.org.Game;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;


public class Menu {

    private Picture menu;

    private Game game;

    // Propriedades do Loading
    private Text dot;
    private Text dot2;
    private Text dot3;
    private Text dot4;
    private Text dot5;
    private Text dot6;

    private Text loading;

    private Rectangle transition;

    public Menu(Game game) {
        menu = new Picture(Map.PADDING, Map.PADDING, "menu.png");
        menu.draw();
        this.game = game;

    }

    public void erase() {
        menu.delete();
    }

    public void loading() {

        transition = new Rectangle(10, 10, 800, 600);
        transition.setColor(Color.BLACK);
        transition.fill();

        loading = new Text(400, 300, "Loading ...");
        loading.setColor(Color.WHITE);
        loading.grow(30, 30);
        loading.draw();





        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        dot = new Text(350, 300, ".");
                        dot.setColor(Color.WHITE);
                        dot.grow(10, 50);
                        dot.draw();

                    }
                },
                10
        );

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        dot2 = new Text(380, 300, ".");
                        dot2.setColor(Color.WHITE);
                        dot2.grow(10, 50);
                        dot2.draw();

                    }
                },
                400
        );

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        dot3 = new Text(410, 300, ".");
                        dot3.setColor(Color.WHITE);
                        dot3.grow(10, 50);
                        dot3.draw();

                    }
                },
                600
        );

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        dot4 = new Text(440, 300, ".");
                        dot4.setColor(Color.WHITE);
                        dot4.grow(10, 50);
                        dot4.draw();

                    }
                },
                800
        );
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        dot5 = new Text(470, 300, ".");
                        dot5.setColor(Color.WHITE);
                        dot5.grow(10, 50);
                        dot5.draw();

                    }
                },
                1000
        );

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        dot6 = new Text(500, 300, ".");
                        dot6.setColor(Color.WHITE);
                        dot6.grow(10, 50);
                        dot6.draw();

                    }
                },
                1200
        );

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        transition.delete();
                        loading.delete();
                        dot.delete();
                        dot2.delete();
                        dot3.delete();
                        dot4.delete();
                        dot5.delete();
                        dot6.delete();

                        // Faz o off do menu para poder mexer o Player
                        game.setMenuOff();
                    }
                },
                3500
        );

    }
}
