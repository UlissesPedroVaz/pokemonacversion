package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Combat implements KeyboardHandler {

    private boolean inCombat;
    private boolean combatConfirm;
    private boolean question;

    private Rectangle timer1, timer2, health1, health2;
    private Picture frame, background;
    private Text quiz, answer;

    private KeyboardControl keyboard;
    private NPC npc;
    private Player player;
    private Map map;
    private Game game;

    public Combat() {

    }


    public void startCombat() {

        if (!inCombat) {
            inCombat = true;

            //delete map
            map.erase();

            //delete player & npc
            npc.erase();
            player.erase();

            //quiz frames
            frame = new Picture(10, 10, "frame2.png");
            frame.draw();

            //quiz time //width time and health
            timer1 = new Rectangle(30, 170, 278, 10);
            timer1.setColor(Color.CYAN);
            timer1.fill();
            timer2 = new Rectangle(29, 169, 279, 11);
            timer2.setColor(Color.BLACK);
            timer2.draw();

            //NPC health //width health
            health1 = new Rectangle(30, 180, 278, 10);
            health1.setColor(Color.RED);
            health1.fill();
            health1 = new Rectangle(29, 179, 279, 11);
            health1.setColor(Color.BLACK);
            health1.draw();

            //Player health //width health
            health2 = new Rectangle(511, 440, 278, 10);
            health2.setColor(Color.RED);
            health2.fill();
            health2 = new Rectangle(510, 439, 279, 11);
            health2.setColor(Color.BLACK);
            health2.draw();

            //Text quiz
            quiz = new Text(60, 120, "text is in the right position");
            quiz.draw();

            //Answers
            answer = new Text(550, 500, "Hello this is a test, to see if the");
            answer.draw();

        }

    }

    public void eraseCombat() {
        frame.delete();
        answer.delete();
        timer1.delete();
        timer2.delete();
        health1.delete();
        health2.delete();
        quiz.delete();
        answer.delete();
    }


    public void question1() {
        

    }

    public void question2() {

    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_1:

            case KeyboardEvent.KEY_2:

            case KeyboardEvent.KEY_3:

            case KeyboardEvent.KEY_4:

            case KeyboardEvent.KEY_Y:

            case KeyboardEvent.KEY_N:

        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
