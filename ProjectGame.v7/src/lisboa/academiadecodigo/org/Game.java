package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Game implements KeyboardHandler {


    // Propriedades
    private Player player;
    private NPC npc;
    private KeyboardControl keyboardControl;
    private Map map;
    private Combat combat;

    private boolean keypressed;

    private int clickedTimes = 0;


    // Instancia os objectos
    public void init() {

        map = new Map();
        npc = new NPC(this, map,100);
        player = new Player(this, map,100);
        keyboardControl = new KeyboardControl(this);
        combat = new Combat();

    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_UP:
                player.moveUp();
                break;

            case KeyboardEvent.KEY_DOWN:
                player.moveDown();
                break;

            case KeyboardEvent.KEY_LEFT:
                player.moveLeft();
                break;

            case KeyboardEvent.KEY_RIGHT:
                player.moveRight();
                break;

            /*case KeyboardEvent.KEY_Y:
                map.erase();
                combat.startCombat();
                System.out.println("Key pressed.");*/

            case KeyboardEvent.KEY_SPACE:
                keypressed = true;

                if ((keypressed && player.getCol() == 2 && player.getRow() == 14 ||
                        player.getCol() == 2 && player.getRow() == 15 ||
                        player.getCol() == 2 && player.getRow() == 16)
                        ||
                        (keypressed && player.getCol() == 28 && player.getRow() == 8 ||
                                player.getCol() == 28 && player.getRow() == 9 ||
                                player.getCol() == 28 && player.getRow() == 10)) {

                    if (clickedTimes == 0) {
                        player.startTalk();
                        clickedTimes++;
                        player.setTalking(true);
                        System.out.println("game is talking: " + player.isTalking());
                    } else {
                        player.endTalk();
                        clickedTimes = 0;
                        player.setTalking(false);
                        System.out.println("game is talking: " + player.isTalking());
                    }
                }

        }
    }



    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                keypressed = false;

        }
    }


}
