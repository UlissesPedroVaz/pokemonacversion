package lisboa.academiadecodigo.org.Map;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.awt.*;

public class Map {

    private Rectangle background;

    private Picture map1, dialog1, dialog2;

    private Text text1, text2;

    public static final int COLS = 8;
    public static final int ROWS = 6;

    public static final int CELL_SIZE = 20;
    public static final int PADDING = 10;

    // Construtor
    public Map() {

        background = new Rectangle(PADDING, PADDING, getWidth(), getHeight());
        map1 = new Picture(PADDING,PADDING,"Background.v2.png");


        background.fill();
        map1.draw();

    }

    public void dialog1(){
        dialog1 = new Picture(200,220,"text.png");
        dialog1.draw();

        text1 = new Text(245,240,"Hello this is a test, to see if the");
        text2 = new Text(245,260,"text is in the right position");
        text1.draw();
        text2.draw();
        text1.grow(10,10);
        text2.grow(10,10);
    }

    public void dialog2(){
        dialog2 = new Picture(400,120,"tex2.png");
        dialog2.draw();

        text1 = new Text(445,140,"Hello this is a test, to see if the");
        text2 = new Text(445,160,"text is in the right position");
        text1.draw();
        text2.draw();
        text1.grow(10,10);
        text2.grow(10,10);
    }

    public void removedialog1(){
        dialog1.delete();
        dialog2.delete();
        text1.delete();
        text2.delete();
    }

    public void erase(){
        map1.delete();
    }

    public void show(){
        map1.draw();
    }


    //GET COL
    public int getCols() {
        return COLS;
    }

    public int getRows() {
        return ROWS;
    }

    // Get width
    public int getWidth() {
        return COLS * CELL_SIZE;
    }

    // Get Height
    public int getHeight() {
        return ROWS * CELL_SIZE;
    }

    // get Cell-size
    public int getCellSize() {
        return CELL_SIZE;

    }



}
