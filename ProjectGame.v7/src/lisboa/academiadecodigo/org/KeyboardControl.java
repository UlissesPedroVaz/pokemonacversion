package lisboa.academiadecodigo.org;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;

public class KeyboardControl {

    Game game;
    Keyboard keyboard;

    public KeyboardControl(Game game){
        this.game = game;
        keyboard = new Keyboard(game);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(left);

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(right);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(up);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(down);


        KeyboardEvent spaceBar = new KeyboardEvent();
        spaceBar.setKey(KeyboardEvent.KEY_SPACE);
        spaceBar.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(spaceBar);

        KeyboardEvent spaceBar2 = new KeyboardEvent();
        spaceBar2.setKey(KeyboardEvent.KEY_SPACE);
        spaceBar2.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        keyboard.addEventListener(spaceBar2);

        KeyboardEvent keyY = new KeyboardEvent();
        keyY.setKey(KeyboardEvent.KEY_Y);
        keyY.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(keyY);

        KeyboardEvent keyN = new KeyboardEvent();
        keyN.setKey(KeyboardEvent.KEY_N);
        keyN.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(keyN);

        KeyboardEvent key1 = new KeyboardEvent();
        key1.setKey(KeyboardEvent.KEY_1);
        key1.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(key1);

        KeyboardEvent key2 = new KeyboardEvent();
        key2.setKey(KeyboardEvent.KEY_2);
        key2.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(key2);

        KeyboardEvent key3 = new KeyboardEvent();
        key3.setKey(KeyboardEvent.KEY_3);
        key3.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(key3);

        KeyboardEvent key4 = new KeyboardEvent();
        key4.setKey(KeyboardEvent.KEY_4);
        key4.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(key4);


    }



}


