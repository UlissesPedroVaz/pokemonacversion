package lisboa.academiadecodigo.org.Characters;

import lisboa.academiadecodigo.org.Game;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {

    private String name;

    private Game game;
    private Map map;

    private boolean isInCombat;
    private boolean isTalking;


    private Picture playerMoveUp;
    private Picture playerMoveDown;
    private Picture playerMoveLeft;
    private Picture playerMoveRight;


    private int col;
    private int row;

    public Player(Game game, Map map) {
        this.game = game;
        this.map = map;


        playerMoveUp = new Picture(80, 20, "p1_up1.png");
        playerMoveDown = new Picture(80, 20, "p1_down1.png");
        playerMoveLeft = new Picture(80, 20, "p1_left1.png");
        playerMoveRight = new Picture(80, 20, "p1_right1.png");


        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());

        System.out.println("col: " + col + " Row: " + row);

    }

    // Apagar o player, todas as 4 imagens:
    public void erasePalyer() {
        playerMoveUp.delete();
        playerMoveDown.delete();
        playerMoveLeft.delete();
        playerMoveRight.delete();

    }

    public void showPlayerDown() {
        playerMoveDown.draw();
    }


    public void moveUp() {

        playerMoveDown.delete();
        playerMoveLeft.delete();
        playerMoveRight.delete();
        playerMoveUp.draw();
        if (row == 0) {

            return;
        }
//position near npc 1 to talk
        if (isTalking && col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16) {

            while (isTalking) {
                playerMoveRight.draw();
                playerMoveUp.delete();
                playerMoveDown.delete();
                playerMoveLeft.delete();
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
//position near npc 2 to talk
        if (col == 28 && row == 8 ||
                col == 28 && row == 9 ||
                col == 28 && row == 10) {

            while (isTalking) {
                playerMoveLeft.draw();
                playerMoveUp.delete();
                playerMoveDown.delete();
                playerMoveRight.delete();
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }

        // Obstacles             -- Board--                 --Tables PCs--                   --Table MCs--
        if (row == 2 && (col < 23 && col > 7) || (row == 19 && (col < 26 && col > 3)) || (row == 4 && col > 26)) {
            return;
        }

        playerMoveUp.translate(0, -map.getCellSize());
        row--;

        // Other moves
        playerMoveDown.translate(0, -map.getCellSize());
        playerMoveLeft.translate(0, -map.getCellSize());
        playerMoveRight.translate(0, -map.getCellSize());


        System.out.println("Col: " + col + " Row: " + row);

    }

    public void moveDown() {

        playerMoveUp.delete();
        playerMoveLeft.delete();
        playerMoveRight.delete();
        playerMoveDown.draw();

        if (row == 25) {
            return;
        }
//position near npc 1 to talk
        if (isTalking && col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16) {

            while (isTalking) {
                playerMoveRight.draw();
                playerMoveUp.delete();
                playerMoveDown.delete();
                playerMoveLeft.delete();
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
//position near npc 2 to talk
        if (col == 28 && row == 8 ||
                col == 28 && row == 9 ||
                col == 28 && row == 10) {

            while (isTalking) {
                playerMoveLeft.draw();
                playerMoveUp.delete();
                playerMoveDown.delete();
                playerMoveRight.delete();
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }

        // Obstacles         --Tables--              --Table MCs
        if (row == 4 && (col < 26 && col > 3) || row == 0 && (col > 26)) {
            return;
        }

        playerMoveDown.translate(0, map.getCellSize());
        row++;

        // Other moves
        playerMoveUp.translate(0, map.getCellSize());
        playerMoveLeft.translate(0, map.getCellSize());
        playerMoveRight.translate(0, map.getCellSize());
        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " + row);

    }

    public void moveLeft() {

        playerMoveUp.delete();
        playerMoveDown.delete();
        playerMoveRight.delete();
        playerMoveLeft.draw();
        if (col == -3) {
            return;
        }
//position near npc 1 to talk
        if (isTalking && col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16) {

            while (isTalking) {
                playerMoveRight.draw();
                playerMoveUp.delete();
                playerMoveDown.delete();
                playerMoveLeft.delete();
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
//position near npc 2 to talk
        if (col == 28 && row == 8 ||
                col == 28 && row == 9 ||
                col == 28 && row == 10) {

            while (isTalking) {
                playerMoveLeft.draw();
                playerMoveUp.delete();
                playerMoveDown.delete();
                playerMoveRight.delete();
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }

        // Obstacles    -- Board--                   --Tables--
        if ((col == 23 && row < 2) || (col == 26 && (row < 19 && row > 4))) {
            return;
        }

        playerMoveLeft.translate(-map.getCellSize(), 0);
        col--;

        // Other moves
        playerMoveUp.translate(-map.getCellSize(), 0);
        playerMoveDown.translate(-map.getCellSize(), 0);
        playerMoveRight.translate(-map.getCellSize(), 0);
        //System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " + row);
    }

    public void moveRight() {

        playerMoveUp.delete();
        playerMoveDown.delete();
        playerMoveLeft.delete();
        playerMoveRight.draw();
        if (col == 33) {
            return;
        }
//position near npc 1 to talk
        if (isTalking && col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16) {

            while (isTalking) {
                playerMoveRight.draw();
                playerMoveUp.delete();
                playerMoveDown.delete();
                playerMoveLeft.delete();
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }
//position near npc 2 to talk
        if (col == 28 && row == 8 ||
                col == 28 && row == 9 ||
                col == 28 && row == 10) {

            while (isTalking) {
                playerMoveLeft.draw();
                playerMoveUp.delete();
                playerMoveDown.delete();
                playerMoveRight.delete();
                System.out.println("player1 is talking: " + isTalking);
                return;
            }

        }

        // Obstacles   -- Board--                   --Tables--                      --Table MCs--
        if ((col == 7 && row < 2) || (col == 3 && (row < 19 && row > 4)) || (col == 26 && (row < 4 && row > 0))) {
            return;
        }

        playerMoveRight.translate(map.getCellSize(), 0);
        col++;

        // Other moves
        playerMoveUp.translate(map.getCellSize(), 0);
        playerMoveDown.translate(map.getCellSize(), 0);
        playerMoveLeft.translate(map.getCellSize(), 0);
        // System.out.println("Col: " + playerMove1.getX() + " Row: " + playerMove1.getY());
        System.out.println("Col: " + col + " Row: " + row);
    }


    public void startTalk() {

        if (isInCombat) {
            return;
        }

        //position near the npc where player can talk
        if (col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16) {
            isTalking = true;
            if (game.getClickedTimes() == 0) {
                System.out.println("fez o dialog 1 NPC");
                map.dialog1NPC();
                return;
            } else if (game.getClickedTimes() == 1){
                System.out.println("fez o dialog 1 do player no player");
                map.dialog1Player();
                return;
            }
            System.out.println("fez o dialog 3");
            map.dialog3NPC();

        }

        if (col == 28 && row == 10 ||
                col == 28 && row == 9 ||
                col == 28 && row == 8) {
            isTalking = true;
            if (game.getClickedTimes() == 0) {
                System.out.println("fez o dialog 1 NPC");
                map.dialog2NPC();
                return;
            } else if (game.getClickedTimes() == 1){
                System.out.println("fez o dialog 2 do player no player");
                map.dialog2Player();
                return;
            }
            System.out.println("fez o dialog 3");
            map.dialog4NPC();

        }
    }








    public void endTalk() {

        //position near the npc where player cant talk
        if (col == 2 && row == 14 ||
                col == 2 && row == 15 ||
                col == 2 && row == 16) {
            map.removedialog1();
            isTalking = false;
            return;
        }

        if (col == 28 && row == 10 ||
                col == 28 && row == 9 ||
                col == 28 && row == 8 ) {
            map.removedialog2();
            isTalking = false;
            return;
        }

    }

    public void setInCombat(boolean inCombat) {
        isInCombat = inCombat;
    }

    public boolean isInCombat() {
        return isInCombat;
    }

    public boolean isTalking() {
        return isTalking;
    }

    public void setTalking(boolean talking) {
        isTalking = talking;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getWidth() {
        return col * 20;
    }

    public int getHeight() {
        return row * 20;
    }


}

