package lisboa.academiadecodigo.org.Characters;

import lisboa.academiadecodigo.org.Combat;
import lisboa.academiadecodigo.org.Game;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class NPC {

    private String name;

    private Game game;
    private Map map;
    private Combat combat;

    // Imagens dos NPCs
    private Picture npc1;
    private Picture npc2;


    private int col = 0;
    private int row = 0;

    public NPC(Game game, Map map) {
        this.game = game;
        this.map = map;

        npc1 = new Picture(180,330, "p1_left1.png");
        System.out.println(" NPC :    Col: " + npc1.getX() + " Row: " + npc1.getY());

        npc1.draw();

        npc2 = new Picture(580,220, "p1_right1.png");
        System.out.println(" NPC :    Col: " + npc2.getX() + " Row: " + npc2.getY());

        npc2.draw();
    }

    // Apaga todos os NPCs:
    public void eraseNPC(){
        System.out.println("Entrou no eraseNPC");
        npc1.delete();
        npc2.delete();

    }

    public void showNPC(){
        System.out.println("Entrou no eraseNPC");
        npc1.draw();
        npc2.draw();

    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

}
