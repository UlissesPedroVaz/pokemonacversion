package lisboa.academiadecodigo.org;

import lisboa.academiadecodigo.org.Characters.NPC;
import lisboa.academiadecodigo.org.Characters.Player;
import lisboa.academiadecodigo.org.Map.Map;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Combat {

    private String name;

    // Booleanos para cada combate
    private boolean inCombat =false;
    private boolean inCombat2 =false;

    private boolean correct;
    private boolean timer;

    private int npcHealth;
    private int playerHealth;

    private Rectangle timer1, timer2, health1, health2, health3, health4;
    private Picture frame, background;
    private Text quiz, answer;

    private KeyboardControl keyboard;
    private Map map;
    private Game game;

    private Player player;
    private NPC npc;


    public Combat(Game game, Map map, Player player, NPC npc) {
        this.game = game;
        this.map = map;
        this.player = player;
        this.npc = npc;
    }

    // Acrescentado:
    // Getter inCombat:
    public boolean isInCombat() {
        return inCombat;
    }

    // Setter de não esta a combater
    public void setNotInCombat(){
        inCombat = false;
    }


    public void startCombat() {

        // Apaga o mapa
        map.erase();
        System.out.println("Fez erase do mapa");

        // Apaga o Player
       player.erasePalyer();
       System.out.println("Fez erase do player");


        // Apaga os NPCS
        npc.eraseNPC();




        if (!inCombat) {
            System.out.println("Entrou no while Combat");
            inCombat = true;

            //quiz frames
            frame = new Picture(10, 10, "frame2.png");
            frame.draw();

            //quiz time //width time and health
            timer1 = new Rectangle(30, 170, 278, 10);
            timer1.setColor(Color.CYAN);
            timer1.fill();
            timer2 = new Rectangle(29, 169, 279, 11);
            timer2.setColor(Color.BLACK);
            timer2.draw();

            //NPC health //width health
            health1 = new Rectangle(30, 180, 278, 10);
            health1.setColor(Color.RED);
            health1.fill();
            health2 = new Rectangle(29, 179, 279, 11);
            health2.setColor(Color.BLACK);
            health2.draw();

            //Player health //width health
            health3 = new Rectangle(511, 440, 278, 10);
            health3.setColor(Color.RED);
            health3.fill();
            health4 = new Rectangle(510, 439, 279, 11);
            health4.setColor(Color.BLACK);
            health4.draw();

            //Text quiz
            quiz = new Text(60, 60, "Question");
            quiz = new Text(60,80,"Question");
            quiz.draw();

            //Answers
            answer = new Text(550, 500, "Hello this is a test, to see if the");
            answer.draw();

            /*combat();
            boolean timer = game.isTimer();
            timer = true;*/
        }
    }

    public void combat(){
        /*while (!timer){
            int timer = timer1.getWidth() - 10;
            timer1.delete();
            timer1 = new Rectangle(timer1.getX(), timer1.getY(), timer, 10);
            timer1.setColor(Color.CYAN);
            timer1.fill();
        }*/

            switch (game.getPressedKey()) {
                case 1:
                    //Wrong answer blueprint
                    System.out.println("Pressed 1");
                    playerHealth = health3.getWidth() - 140;
                    health3.delete();
                    health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                    health3.setColor(Color.RED);
                    health3.fill();
                    if(playerHealth <= 0){
                        setNotInCombat();
                        eraseCombat();

                        game.showALL();
                    }
                    System.out.println(npcHealth);
                    break;
                case 2:
                    //Right answer blueprint
                    System.out.println("Pressed 2");
                    npcHealth = health1.getWidth() - 140;
                    health1.delete();
                    health1 = new Rectangle(health1.getX(), health1.getY(), npcHealth, 10);
                    health1.setColor(Color.RED);
                    health1.fill();
                    System.out.println(npcHealth);
                    break;
                case 3:
                    System.out.println("Pressed 3");
                    playerHealth = health3.getWidth() - 140;
                    health3.delete();
                    health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                    health3.setColor(Color.RED);
                    health3.fill();
                    break;
                case 4:
                    System.out.println("Pressed 4");
                    playerHealth = health3.getWidth() - 140;
                    health3.delete();
                    health3 = new Rectangle(health3.getX(), health3.getY(), playerHealth, 10);
                    health3.setColor(Color.RED);
                    health3.fill();
            }


        /*if(game.getPressedKey() == 1){
            System.out.println("Pressed 1");
        }
        if (game.getPressedKey() == 2){
            System.out.println("Pressed 2");
        }
        if (game.getPressedKey() == 3){
            System.out.println("Pressed 3");
        }
        if (game.getPressedKey() == 4){
            System.out.println("Pressed 4");
        }*/
    }



    public void eraseCombat() {
       frame.delete();
       timer1.delete();
       timer2.delete();
       health1.delete();
       health2.delete();
       health3.delete();
       health4.delete();
    }


}
